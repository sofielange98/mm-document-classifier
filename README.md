# mm-document-classifier

Java program to take in a text file and classify it as either an essay, ebook, or other.

___DOCUMENT CLASSIFIER___
mm-classify-model-v3.ser.gz is a classifier based on ebooks, essays, and other to weed out academic documents from risk checking

To compile ---

From mm-document-classifier directory

javac -cp ".:./*" ClassifierDemo.java
	

COMMAND LINE OPTIONS
files -- 
if args[0] == files then you will be prompted to input filenames individually for testing

perf pathtotestfile e.g. perf examples/mm-classify-test-v3 --
will display results of classifier like accuracy and time taken to classify all data on input file

test directoryname -- e.g. test risky
will analyze all files in a given directory and output the class of the document and its categorization as either academic or non-academic

to run ---
java -cp ".:./*" ClassifierDemo args

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

___RISK CLASSIFIER___
suicide_classify_model.ser.gz
this is a classifier made using reddit suicide watch to assess a document for self-harm/suicide risk

Usage is virtually identical to ClassifierDemo, but classes are limited to 'risk' or 'norisk'
Also, there are two options for test files
-suicide_classify_test - contains 1600 samples, 800 no risk and 800 risk
-risky_class - contains all risky samples, 4882 reddit posts
To compile ---
From mm-document-classifier directory

javac -cp ".:./*" ClassifierDemo.java

to run ---

java -cp ".:./*" ClassifierDemo args

CLASSIFIER FINAL VERSION

~~~ TRAIN SET 6090~~~

(ESSAY) 2030
sample student essays 1885 
hand-picked 145 

(EBOOK) 2030
sample gutenberg ebooks 2030

(OTHER) 2030 
random reddit 640
reddit suicide watch 640
enron emails 640
letters 110

~~~ TEST SET 2101~~~

(ESSAY) 700 
sample student essays 630
hand picked 70

(EBOOK) 700 
gutenberg 700

(OTHER) 701
random reddit 217
reddit suicide watch 217
enron emails 217
letters 50 

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

the classifier contained in 

suicide_classify_model.ser.gz

~~~TRAIN SET~~~8000

(RISK)
reddit suicide watch posts
4000 samples

(NORISK)
random reddit
1000 samples

student essays
1000 samples

enron emails
1000 samples

gutenberg ebooks
1000 samples

~~~TEST SET~~~ 1600

(RISK)
reddit suicide watch posts
800 samples

(NORISK)
random reddit
200 samples

student essays
200 samples

enron emails
200 samples

gutenberg ebooks
200 samples

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

The following is an overview of the data used to train and test the model contained in 

mm-classify-model-v2.ser.gz

~~~TRAIN SET~~~ 9999
all documents reduced to avg size of 350 words

(EBOOKS) 
gutenberg ebooks
3333 samples

(ESSAYS) 
sample student essays
3333 samples

(OTHER)
enron emails 
1111 samples

random reddit 
1111 samples

reddit suicide watch 
1111 samples

~~~TEST SET~~~ 2106

(EBOOKS) 
gutenberg ebooks 
702 samples

(ESSAYS)
sample student essays 
702 samples

(OTHER)
enron emails 
234 samples

random reddit 
234 samples

reddit suicide watch
234 samples
