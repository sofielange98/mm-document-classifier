
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.util.stream.Stream;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.lang.RuntimeException;
import java.lang.Object;
import java.util.Scanner;

import edu.stanford.nlp.classify.Classifier;
import edu.stanford.nlp.classify.ColumnDataClassifier;
import edu.stanford.nlp.classify.LinearClassifier;
import edu.stanford.nlp.ling.Datum;
import edu.stanford.nlp.objectbank.ObjectBank;
import edu.stanford.nlp.util.ErasureUtils;
import edu.stanford.nlp.util.Pair;

class ClassifierDemo {

  private static String where = "";
  private static ColumnDataClassifier cdc;

  public static void main(String[] args) throws Exception {
    if (args.length > 0) {
      where = System.getProperty("user.dir") + File.separator;
    }

    //System.out.println("Training ColumnDataClassifier");
    //ColumnDataClassifier cdc = new ColumnDataClassifier(where + "examples/cheese2007.prop");
    //cdc.trainClassifier(where + "examples/cheeseDisease.train");
    //System.out.println("Attempting to load classifier!");
    //cdc = ColumnDataClassifier.getClassifier(where + "mm-classify-model-v2.ser.gz"); // version 2
    //cdc2 = ColumnDataClassifier.getClassifier(where + "suicide_classify_model.ser.gz"); //risk vs norisk
    //cdc3 = ColumnDataClassifier.getClassifier(where + "mm-classify-model.ser.gz"); //original version

/*
This is where the classifier is loaded from the external file. cdc now contains a classifier which performs according the the model stored in mm-classify-model-v3.ser.gz
*/
    cdc = ColumnDataClassifier.getClassifier(where + "mm-classify-model-v3.ser.gz"); //final version
    //System.out.println("Loaded!");
    //command line argument checking 
    if(args.length>0){
      if(args[0].equals("perf")){
        try{
          perform(cdc, args[1]);
        }
        catch(IOException ex){
          System.out.println("Couldn't locate file "+args[2]);
          System.out.println("Argument1: (perf if evaluating performance), Argument3: location of test file e.g. example/mm-classify-test-v3");
        }
      }
      else if(args[0].equals("test")){
        try{
          onDir(args[1]);
        }
        catch(IOException ex){
          System.out.println("Couldn't locate file "+args[1]);
          System.out.println("Argument1: (test if testing on directory of files), Argument2: directory to test on eg nonacademic");
        }
      }
      else if(args[0].equals("files")){
          analyzeInputFile();
      }
      else{
        System.out.println("Please try again -- options include : \nfiles (test individual files)\ntest directory (classify all docs in given directory)\nperf testfile (see performance on given test data");
      }
    }
  }
  /*
  perform will test the performance of the classifier on a given test file
  mm-classify-test-v3 contains 2,101 test data points
  testClassifier will analyze the text in the line, classify the data and then compare its output to the assigned class
  data is formatted as, per line
  class\tdata
  perform outputs this process as well as the amount of time taken to classify all the data
  */
  private static void perform(ColumnDataClassifier cdc, String testfile)
  throws IOException, ClassNotFoundException{
    System.out.println("Testing accuracy and timing of ColumnDataClassifier");
    long startTime = System.nanoTime();
    Pair<Double, Double> performance = cdc.testClassifier(where + testfile);
    long endTime = System.nanoTime();
    System.out.printf("Accuracy: %.3f; macro-F1: %.3f%n", performance.first(), performance.second());
    long durr = (endTime-startTime)/1000000;
    System.out.printf("Classified 2106 documents in %d milliseconds.\n",durr);
  }
/*
classifyText will take in a string of text data and return the documents class according to that classifier
*/
  private static String classifyText(String txtdata)
  {
    String cls;
    String[] data = new String[2];
    data[0] = "none";
    data[1] = txtdata;
    Datum<String,String> d = cdc.makeDatumFromStrings(data);
    cls = cdc.classOf(d); 
    return cls;
  }
private static void analyze(String filename)
  throws IOException, ClassNotFoundException{
    String[] data = new String[2];
    String cls,academic;
    Datum<String,String> d;
    int numwords = 0;
    data[0] = "none";
      data[1] = "";
      numwords = 0;
      long startTime = System.nanoTime();
      try{  
              //parse the file, taking only the first ~500 words
              for (String line : ObjectBank.getLineIterator(filename, "utf-8")) {
                if(!line.equals('\n'))
                  {
                    String[] linewords = line.split(" ");
                    if(numwords+linewords.length>=500){
                      int rmn = 500 - numwords;
                      for(int i = 0; i<rmn; i++){
                        data[1] += " " +linewords[i];
                      }
                      numwords = 500;
                    }
                    else {
                      numwords += line.split(" ").length;
                   //System.out.println("Num Words "+ numwords);
                      data[1] += " " + line;
                      if(numwords>=500)
                        break;
                    }
                  }
                }
              System.out.printf(data[1]); //should contain the data we want to classify 
              System.out.println("\n*     *     *     *     *");

              cls = classifyText(data[1]);

              if(cls.equals("other"))
                academic = "NON-ACADEMIC";
              else
                academic = "ACADEMIC";

              System.out.printf("Class of Document ==>  %s\n", cls);
              System.out.println("This document has been classified as : " +academic);
              System.out.println("*     *     *     *     *");
              long endTime = System.nanoTime();
              long durr = (endTime - startTime)/1000000;
              System.out.printf("Time to compute class: %d milliseconds\n", durr);
      }
      catch(RuntimeException ex){
        System.out.println("Sorry, couldn't find that file, try again."+where+filename);
      }
  }
  private static void onDir(String dirName)
  throws IOException, ClassNotFoundException{
    String[] data = new String[2];
    String cls,cls2,cls3,academic,filename;
    Datum<String,String> d,d2,d3;
    int numwords = 0;
    data[0] = "none";
  try (Stream<Path> walk = Files.walk(Paths.get(System.getProperty("user.dir")+"/"+dirName))) {
      walk.forEach(filePath -> {
        if (Files.isRegularFile(filePath)) {
            System.out.println(filePath);
            try{analyze(filePath.toString());}
            catch(IOException|ClassNotFoundException e){
              System.out.println("Uh oh!");
            }
          }
    });

  } catch (IOException e) {
    e.printStackTrace();
  }
  }
/*
analyzeInputFile will repeatedly ask for an input file and will attempt to get the first ~500 words from the file, and then print the class
current implementation assigns class based on classifier 1 and 2, but shows what would have been assigned by original classifier as well
*/
  private static void analyzeInputFile()
  throws IOException, ClassNotFoundException{
    String[] data = new String[2];
    String cls,academic,filename;
    Datum<String,String> d;
    int numwords = 0;
    data[0] = "none";
    Scanner input = new Scanner(System.in);
    do{
      System.out.println("Please enter file you would like analyzed:");
      filename = input.nextLine();
      data[1] = "";
      numwords = 0;
      long startTime = System.nanoTime();
      try{  
              for (String line : ObjectBank.getLineIterator(where + filename, "utf-8")) {
                if(!line.equals('\n'))
                  {
                    String[] linewords = line.split(" ");
                    if(numwords+linewords.length>=500){
                      int rmn = 500 - numwords;
                      for(int i = 0; i<rmn; i++){
                        data[1] += " " +linewords[i];
                      }
                      numwords = 500;
                    }
                    else {
                      numwords += line.split(" ").length;
                   //System.out.println("Num Words "+ numwords);
                      data[1] += " " + line;
                      if(numwords>=500)
                        break;
                    }
                  }
                }
              System.out.printf(data[1]);
              System.out.println("\n*     *     *     *     *");

              d = cdc.makeDatumFromStrings(data);
              cls = cdc.classOf(d); 

              if(cls.equals("other"))
                academic = "NON-ACADEMIC";
              else
                academic = "ACADEMIC";

              System.out.printf("Class of Document  ==>  %s\n", cls);
              System.out.println("This document has been classified as : " +academic);
              System.out.println("*     *     *     *     *");
              long endTime = System.nanoTime();
              long durr = (endTime - startTime)/1000000;
              System.out.printf("Time to compute class: %d milliseconds\n", durr);
      }
      catch(RuntimeException ex){
        System.out.println("Sorry, couldn't find that file, try again.");
      }
    }while(!filename.equals("quit"));
  }
}
